FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /HelloMvc

# copy csproj and restore as distinct layers
COPY *.csproj .
RUN dotnet restore HelloMvc.csproj

# copy everything else and build app
COPY . ./

RUN dotnet publish HelloMvc.csproj -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /HelloMvc
COPY --from=build /HelloMvc/out ./

ENV ASPNETCORE_URLS=http://*:$PORT
# CMD dotnet HelloMvc.dll
ENTRYPOINT [ "dotnet", "HelloMvc.dll" ]
